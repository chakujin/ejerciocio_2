﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Dino : MonoBehaviour
{
    private Rigidbody2D rb2d = null;
    private float move = 0f;
    public float maxS = 11f;
    private bool jump;
    public float fuerzaSalto = 5.0f;
    public bool IsGrounder = false;
    public GameObject graphics;
    private float escalaactual;
    private bool attack;
    public Animator animator;
    private bool isDead = false;
    // Use this for initialization
    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        //rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);
        rb2d.AddForce(Vector2.right * move * maxS);

            animator.SetFloat("Velocidad", Math.Abs(rb2d.velocity.x));
    }

    private void Update()
    {
        move = Input.GetAxis("Horizontal");
        jump = Input.GetKeyDown(KeyCode.Space);
        attack = Input.GetButtonDown("Fire1");
        if (attack && IsGrounder)
        {
            move = 0f;
            jump = false;
            animator.SetBool("attack", attack);
        }
        else{
           // animator.SetBool("attack", false);
        }
        if (jump && IsGrounder)
        {
           // Debug.LogError("Salta");
            jump = false;
            rb2d.AddForce(Vector2.up * fuerzaSalto, ForceMode2D.Impulse);
        }

        if (isDead) { return; }


         
        escalaactual = graphics.transform.localScale.x;

        if (move > 0 && escalaactual<0)
        {
            graphics.transform.localScale = new Vector3(1, 1, 1);

        }else if (move < 0 && escalaactual > 0)
        {
            graphics.transform.localScale = new Vector3(-1, 1, 1);
        } 

    }
    private void OnTriggerExit2D(Collider2D trampo)
    {
        rb2d.AddForce(Vector2.up * fuerzaSalto, ForceMode2D.Impulse);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        IsGrounder = true;
    }


}